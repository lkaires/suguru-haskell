# Relatório

O jogo [Suguru](https://www.janko.at/Raetsel/Suguru/index.htm) é uma variação de Sudoku onde existem variações nas áreas e no tamanho do tabuleiro. O tabuleiro pode assumir outros tamanhos, contanto que se mantenha como um quadrado; as áreas não precisam ser o 3x3 padrão de Sudoku, podendo assumir formas variadas e ter tamanhos variados. Um exemplo de tabuleiro é o seguinte (código 001):

![Exemplo Suguru 6x6](/imgs/suguru-example-incomplete.png)

## Informando o tabuleiro

Ao rodar o código, pode-se passar o tabuleiro de duas formas: por um arquivo ou por prompts na linha de comando. Ao passar um arquivo, os prompts apenas lerão do arquivo ao invés da linha de comando, portanto a formatação do tabuleiro se mantém a mesma. Um exemplo pode ser encontrado nesse repositório (representando o tabuleiro mostrado acima).

A entrada se dá da seguinte forma:

1. (Tamanho do lado do tabuleiro)
2. (Cada uma das linhas do tabuleiro, com valores já preenchidos ou 0 para células vazias)
3. (Quantidade de áreas do tabuleiro)
4. (Cada área do tabuleiro, passando cada célula como coordenadas da forma x-y e espaço entre cada célula)

Como demonstrado no seguinte exemplo:

![Exemplo entrada 3x3](/imgs/entry-example.png)

## Código

### Módulos

O código conta com 4 módulos auxiliares: `Keyboard`, `ManipStr`, `ManipList`e `Board`.

* `Keyboard` lida com as entradas do teclado e suas conversões, para conseguir pegar as informações do tabuleiro;
* `ManipStr` lida com manipulação de strings para outros tipos, fazendo as conversões necessárias das informações do tabuleiro;
* `ManipList` lida com manipulação de listas variadas, editando listas ou extraindo informações dependendo das necessidades do código;
* `Board` lida com todo tipo de manipulação do tabuleiro, tanto pegando informações de células ou áreas quando editando o tabuleiro. De forma geral apresenta funções pequenas e não muito complexas.

### Lógica

A estratégia de resolução seguida foi a seguinte:

1. Para cara célula vazia, marque as possibilidades;
2. caso existam células vazias que possuam apenas um valor em possibilidades, preencha essas células com o valor;
3. verifique para cada valor ainda não preenchido em uma área se existe apenas uma célula que o tenha nas possibilidades e, caso exista, preencha a célula com o valor;
4. repita até preencher todo o tabuleiro.

Praticamente toda a lógica do código se encontra no arquivo `main`. Nesse arquivo, existem funções para achar todos os valores possíveis de uma célula e para achar quando um valor é possível em apenas uma das células de uma área.

* Para achar todos os valores possíveis de uma célula, verifica-se os valores preenchidos nas células vizinhas e nas outras células da mesma área. Então marca a célula com todos valores de 1 até o tamanho da área, retirando os valores que não são mais possíveis. Isso é feito nas funções `playRound` e `getImpossible` em `main`.
* Para achar quando um valor se encaixa em apenas uma célula de uma área, se constrói uma lista onde cada elemento é a lista de possibilidades de uma célula. Verifica-se essa lista para cada valor possível em uma área (de 1 até o tamanho da área), vendo se apenas uma célula possui tal valor em sua lista de possibilidades. Caso isso seja verdade, marca-se a célula com o valor. A lógica é implementada pela função `markAllAreas`, que passa por cada área do tabuleiro chamando `markUniqueArea`; essa função, por sua vez, pega as listas de possibilidades e verifica quais valores são possíveis em apenas uma célula e passa essas informações para `markAllUnique`; então para cada valor a ser marcado, `markAllUnique` chama `markUnique`.

### Retorno

Após o tabuleiro ser processado pelo código, é retornado da seguinte forma: as quatro primeiras linhas representam os prompts de entrada do tabuleiro, então o programa printa o tabuleiro original, a quantidade de rodadas necessárias de processamento seguindo o algoritmo e o tabuleiro completo.

Um tabuleiro é representado da seguinte forma: `Solve`; seguido de uma lista onde cada elemento representa uma linha do tabuleiro, em ordem; então uma lista onde cada elemento representa uma área, traduzida como uma lista de coordenadas `(x,y)`.

O exemplo a seguir mostra a resolução de um tabuleiro 6x6:

![Exemplo saída 6x6](imgs/output.png)

![Exemplo Suguru 6x6 completo](imgs/suguro-example-complete.png)

## Dificuldades

A dificuldade principal para a completude do trabalho foi a própria linguagem: Haskell. Percebeu-se um problema para a legibilidade do código, o que dificulta a organização do mesmo. Também se teve dificuldade no entendimento de Monads, o que dificultou o retorno de informações adicionais na linha de comando, por forma de prints.

Em relação à lógica do programa, não foi possível a implementação de técnicas de *chute* e backtracking. Por isso, o código se encontra incompleto e, em casos de tabuleiros onde em algum momento o valor das células não é óbvio e seja preciso *chutar*, o programa para e retorna o tabuleiro incompleto, preenchido até tal momento.

---

[Apresentação](https://youtu.be/Hy9Idqj1I0o)
[Repositório no GitLab](https://gitlab.com/lkaires/suguru-haskell)

###### Helena Kunz Aires (17100521)
