module Board (Cell (Fixed, Possible),
              Board (Solve),
              Grid,
              Row,
              Area,
              getBoard,
              markCell,
              getCell,
              getArea,
              getAreaFixed,
              getNeighbourFixed,
              neighbours,
              updateCell,
              updateBoard,
              makeBoard,
              getAreaPossible
             ) where

import Keyboard
import ManipStr
import ManipList


-- Define o que é uma célula
data Cell = Fixed Int | Possible [Int]
    deriving Eq
type Row = [Cell]
type Grid = [Row]
type Area = [(Int,Int)]
-- Define o que é um tabuleiro
data Board = Solve Grid [Area]
    deriving (Show, Eq)

instance Show Cell where
    show (Fixed a) = show a
    show (Possible _) = show 0


-- Helpers

-- Dado um inteiro, retorna uma célula vazia ou já preenchida
cell :: Int -> Cell
cell 0 = Possible []
cell x = Fixed x

-- Dado uma lista de tuplas, retorna uma área
area :: [(Int,Int)] -> Area
area x = x

-- Pega as informações de cada célula do tabuleiro
getGrid :: IO Grid
getGrid = do
    putStrLn "Tamanho do lado do tabuleiro (int):"
    size <- getInt
    putStrLn "Linhas do tabuleiro, com 0 sendo vazio (int ... int):"
    strGrid <- getNLines size
    let intGrid = map (\x -> map (\y -> read y :: Int) x) $ map splitStr strGrid
    let grid = map (\x -> map (\y -> cell y) x) intGrid
    return grid

-- Pega as informações das áreas do tabuleiro
getAreas :: IO [Area]
getAreas = do
    putStrLn "Quantidade de áreas (int):"
    numAreas <- getInt
    putStrLn "Áreas (x-y ... x-y):"
    strAreas <- getNLines numAreas
    let tupleAreas = map strToIntTuples strAreas
    let areas = map area tupleAreas
    return areas

-- Retorna o valor de uma célula marcada ou 0
getCellValue :: Cell -> Int
getCellValue (Fixed a) = a
getCellValue (Possible _) = 0

-- Lista das posições dos vizinhos
-- Recebe a posição desejada e no tamanho do tabuleiro
neighbours :: Int -> (Int,Int) -> Area
neighbours s (x,y)
    | x == s && y == s = [(x-1,y-1),(x,y-1),
                          (x-1,y)]
    | x == s && y == 1 = [(x-1,y),(x-1,y+1),
                          (x,y+1)]
    | x == s = [(x-1,y-1),(x,y-1),
                 (x-1,y),
                 (x-1,y+1),(x,y+1)]
    | x == 1 && y == s = [(x,y-1),
                          (x+1,y-1),(x+1,y)]
    | y == s = [(x-1,y-1),(x,y-1),(x+1,y-1),
                 (x-1,y),(x+1,y)]
    | x == 1 && y == 1 = [(x+1,y),
                          (x,y+1),(x+1,y+1)]
    | x == 1 = [(x,y-1),(x+1,y-1),
                (x+1,y),
                (x,y+1),(x+1,y+1)]
    | y == 1 = [(x-1,y),(x+1,y),
                (x-1,y+1),(x,y+1),(x+1,y+1)]
    | otherwise = [(x-1,y-1),(x,y-1),(x+1,y-1),
                   (x-1,y),(x+1,y),
                   (x-1,y+1),(x,y+1),(x+1,y+1)]

-- Atualiza uma célula em uma linha
updateRow :: Row -> Int -> Cell -> Row
updateRow r y c
    | y == 1 = [c] ++ (tail r)
    | y == (length r) = (init r) ++ [c]
    | otherwise = ((take (y-1) r) ++ [c]) ++ (drop y r)

-- Atualiza uma célula em uma grid
updateGrid :: Grid -> (Int,Int) -> Cell -> Grid
updateGrid g (x,y) c
    | x == 1 = [(updateRow (head g) y c)] ++ (tail g)
    | x == (length g) = (init g) ++ [(updateRow (last g) y c)]
    | otherwise = ((take (x-1) g) ++ [(updateRow (g !! (x-1)) y c)]) ++ (drop x g)

-- Retorna a lista de possibilidades de uma célula
getPossible :: Cell -> [Int]
getPossible (Fixed a) = [a]
getPossible (Possible a) = a


-- Functions

-- Pega as informações do tabuleiro e o retorna como Board
getBoard :: IO Board
getBoard = do
    grid <- getGrid
    areas <- getAreas
    let ret = makeBoard grid areas
    return ret

-- Caso tenha apenas uma possibilidade, preenche a célula
markCell :: Cell -> Cell
markCell (Fixed a) = Fixed a
markCell (Possible a)
    | length a == 1 = Fixed (head a)
    | otherwise = Possible a

-- Pega célula específica em um tabuleiro dada uma posição
getCell :: Board -> (Int, Int) -> Cell
getCell (Solve g a) (x,y) = (g !! (x-1)) !! (y-1)

-- Retorna área de uma célula em um tabuleiro
getArea :: Board -> (Int,Int) -> Area
getArea (Solve g (a:b)) p
    | elem p a = a
    | otherwise = getArea (Solve g b) p

-- Retorna os valores já marcados nas células de uma área
getAreaFixed :: Board -> Area -> [Int]
getAreaFixed b a = filter (\x -> x /= 0) $ map getCellValue (map (\x -> getCell b x) a)

-- Retorna os valores já marcados nas células vizinhas
getNeighbourFixed :: Board -> (Int,Int) -> [Int]
getNeighbourFixed (Solve g a) (x,y) = getAreaFixed (Solve g a) (neighbours (length g) (x,y))

-- Atualiza a lista de possibilidades de uma célula
-- Preenche a lista com 1:n, sendo n o tamanho da área, e retira os que não são possíveis
updateCell :: Cell -> Int -> [Int] -> Cell
updateCell (Fixed a) _ _ = (Fixed a)
updateCell (Possible _) n a = (Possible (removeFromList [1..n] a))

-- Atualiza uma célula em um tabuleiro
updateBoard :: Board -> (Int,Int) -> Cell -> Board
updateBoard (Solve g a) (x,y) c = Solve (updateGrid g (x,y) c) a

-- Dado um grid e uma lista de áreas, retorna um tabuleiro
makeBoard :: Grid -> [Area] -> Board
makeBoard g a = Solve g a

-- Dada uma área, retorna uma lista das possibilidades das células não marcadas
getAreaPossible :: Board -> Area -> [[Int]]
getAreaPossible board area = map (\x -> getPossible (getCell board x)) area
