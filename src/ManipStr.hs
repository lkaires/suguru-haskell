module ManipStr (splitStr,
                 strToIntTuples
                ) where

import Data.Function (fix)


-- Helpers

-- Quebra a string em duas partes no primeiro " " ou "-" e retorna a primeira
breakStr :: [Char] -> [Char]
breakStr "" = []
breakStr (h:t)
    | h == ' ' = []
    | h == '-' = []
    | otherwise = h : breakStr t

-- Quebra a string em duas partes no primeiro " " ou "-" e retorna a segunda
restStr :: [Char] -> [Char]
restStr "" = []
restStr (h:t)
    | h == ' ' = t
    | h == '-' = t
    | otherwise = restStr t


-- Functions

-- Quebra a string em várias partes, a cada " " ou "-", e retorna uma lista
splitStr :: [Char] -> [[Char]]
splitStr "" = []
splitStr str = breakStr str : splitStr (restStr str)

-- Converte uma string em lista de tuplas de inteiro
-- Quebra a string em várias partes, a cada " " ou "-", e pareia 2 a 2
strToIntTuples :: [Char] -> [(Int,Int)]
strToIntTuples a = fix (\f (a:b:c) ->
        if c == [] then (a,b):[]
        else (a,b):(f c)
    ) (map (\x -> read x :: Int) (splitStr a))
